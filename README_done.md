Задание CI/CD:
1. Создать личный репозиторий, содержащий код проекта блога djangogirls
2. Добавить в этот репозитрий докерфайл для сборки проекта
3. Задачи усложненные:
- обеспечить хранение данных на вашем компьютере
- изменить Title сайта в процессе деплоя на версию этого деплоя
- изменить цвет фона в заголовке при каждом коммите


В качестве раннера была использована ВМ на Yandex.Cloud
Команды для раннера:
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get update
sudo apt-get install gitlab-runner
sudo apt-get install     ca-certificates     curl     gnupg     lsb-release
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo addgroup docker
sudo adduser gitlab-runner docker
sudo gitlab-runner register
sudo vi /etc/gitlab-runner/config.toml
### был изменен раздел раннер docker
[[runners]]
  name = "docker"
  url = "https://gitlab.com/"
  token = "kvXwWgcSLbqgUf8VZ_a5"
  executor = "docker"
  environment = ["DOCKER_TLS_CERTDIR="]
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:3-alpine"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0

